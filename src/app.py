from flask import Flask, Response, request
from datetime import datetime
import json

# ToDo: fortify this to use a production ready server such as Gunicorn
app = Flask(__name__)

@app.route('/')
def json_response():
    # get the timestamp and ip for the response
    now = datetime.now()
    formatted_now = now.__str__()
    ip_addr = request.remote_addr

    # create json response
    time_ip = {"timestamp": formatted_now, "ip": ip_addr}
    
    return Response(json.dumps(time_ip), mimetype='application/json')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
