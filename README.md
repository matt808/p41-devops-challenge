# p41-devops-challenge

This project creates a webserver using Python flask that returns the current timestamp and requester's ip address as JSON. It has a Docker image and a Kubernetes manifest to run it as a microservice. A production ready server was not used in the interest of getting this experiment done on a short time frame.  
Here is what a sample response looks like. 
   
![sample response](/images/response.png)

## Basic description
* src/app.py creates the flask service with the logic to get and return the timestamp and ip as JSON
* requirements.txt provides the required imports to build the app
* Dockerfile is used to create the image and container for the service
* available on hub.docker.com - pull mattman70/microserver:v1.1
* microserver.yaml is used in the Kubernetes cluster to create the deployment and service