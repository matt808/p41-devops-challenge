FROM python:3.8-slim-buster

# vars to set up app user and group
ENV USER=appuser
ENV GROUP=appgroup
ENV UID=12345
ENV GID=23456

RUN addgroup --system --gid $GID $GROUP
RUN adduser --system --no-create-home --uid $UID --ingroup $GROUP $USER

RUN mkdir /app
WORKDIR /app

# copy the dependencies file and install
COPY requirements.txt .
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working directory
COPY src/ .

# make sure app doesn't run as root
RUN chown -R appuser:appgroup /app
USER appuser

# run app
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
